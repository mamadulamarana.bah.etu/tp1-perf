# TP 1: Performance d'une bibliothèque d'entrées/sorties tamporisées

Pour travailler sur ce projet, il faut d'abord en faire un copie privé
en cliquant sur `fork` (button en haut à droite). Si vous voulez que
votre intervenant puisse corriger votre projet, il faut l'ajouter à
votre dépot après le fork.

Les TPs ne sont pas à rendre, ils ne seront pas notés. Ils servent
pour apprendre et pour s'entrainer à l'examen. Il est dans votre
interêt de venir regulierement aux séance de TPs, de travailler et de
demander des corrections à l'intervenant de votre groupe qui est la
pour vous aider à progresser.

## Exercices

### Exercice 1 : Influence de la taille des tampons

Il s'agit d'écrire une version de la commande `cat` utilisant
directement les appels systèmes `read()` et `write()`. Le tampon est
alloué dynamiquement au début de l'exécution, sa taille est définie
par la variable d'environnement `MCAT_BUFSIZ`. Cette version sera
nommée `mcat-scd` (mon `cat`, *sc* pour *system call* - appel
système, *d* pour allocation dynamique du tampon). On invoquera
successivement la commande en utilisant des tampons de 1 octet à
8 Mo en doublant la taille à chaque fois.

1. Donnez une implantation de `mcat-scd`;
2. Testez cette commande sur un fichier de taille conséquente en
   entrée, au moins quelques dizaines de Mo.  On se souciera aussi du
   type du système de fichiers sur lequel réside ce fichier (option
   `-T` de la commande `df`). La sortie pourra être redirigée vers
   `/dev/null`.
3. Visualisez les résultats par une courbe en utilisant la commande `gnuplot`.

Qu'est-ce qu'il faut s'attendre comme resultat ?

Les performances doivent augmenter (c'est à dire les temps d'exécution
doivent diminuer) en augmentant la taille du tampon, jusqu'à un point
de minimum. En augmentant encore la taille du tampon, les temps
d'exécution pourront augmenter à nouveau. Verifier que c'est le cas
avec vos courbes.


### Exercice 2 : Version appel système optimale

Implantez maintenant une version `mcat-scs` (suffixe `s` pour
*static*, statique, qui repose sur l'utilisation d'un tampon alloué
statiquement de la taille optimale déterminée à l'étape précédente.


### Exercice 3 : En utilisant la bibliothèque standard

Implantez une version `mcat-lib` qui utilise les primitives `fgetc()`
et `fputc()` de la bibliothèque standard d'entrées/sorties et comparez
les temps d'exécution avec la version précédente.


### Exercice 4 : Écritures en mode synchronisé

Afin de mettre en évidence l'influence des tampons systèmes, comparez
la version précédente `mcat-scs` avec une version `mcat-osync` qui
positionne l'attribut `O\_SYNC` sur le descripteur de la sortie
standard. 

### Exercice 5 : Synchronisation finale

Pour terminer, produisez `mcat-fsync`, version modifiée de
`mcat-scs` qui vide les tampons disque suite aux
écritures par un appel à `fsync()`, et comparez les deux. 
  

## Outils pour resoudre les exercices 

### Comment mesurer le temps

La commande `time` affiche le temps d'exécution d'une
commande donnée en paramètre. Trois informations sont reportées
(voir aussi l'option `-p`) :

- le temps écoulé entre le début et la fin de la commande
  (*elapsed time* ou *real time*) ;
- le temps processeur utilisé en mode utilisateur (*user time*) ;
- le temps processeur utilisé en mode système pour le compte
  du programme (*system time* ou *kernel time*).

Attention, il existe aussi une commande interne du shell nommée
`time` qui reporte ces informations sous un autre format.
La commande `time` qui nous intéresse est souvent placée
dans `/usr/bin/time`. Cette version comporte une option
`-f` permettant de spécifier le format d'affiche du
résultat à la manière de `printf`.


### Les scripts shell

Un script shell est une suite de commandes shell regroupées dans un
fichier dont l'exécution invoquera un shell avec la suite des
commandes du fichier.  Copiez dans un fichier `script.sh` le
contenu suivant:
```bash
#!/bin/bash
# Exemple de script shell qui mesure le temps d'exécution de
# "macommande" dans les cas où la variable d'environnement
# MCAT_BUFSIZ vaut 1, 16 et 256

export MCAT_BUFSIZ
MCAT_BUFSIZ=1   /usr/bin/time -f '%e %U %S' macommande
MCAT_BUFSIZ=16  /usr/bin/time -f '%e %U %S' macommande
MCAT_BUFSIZ=256 /usr/bin/time -f '%e %U %S' macommande
```

Il s'agit ensuite de rendre exécutable ce fichier pour pouvoir le
déclencher comme un exécutable normal:

```bash
$ chmod +x script.sh
$ ./script.sh
[...]
```

Les shells disposent par ailleurs, comme les langages de
programmation plus classiques, de structures de contrôle.
En `bash` par exemple, vous pouvez faire une boucle:
```bash
for((i = 0; i < 10; i++)); do
    printf '%d' "$i"
  done
```
pour éviter des duplications de code.
Consultez la page de manuel de `bash` pour en savoir plus.

### Tracer des courbes avec `gnuplot``

`Gnuplot` est un utilitaire de tracé de courbes. Dans sa
version interactive, `gnuplot` accepte des commandes au
prompt. Ces commandes acceptent parfois des arguments. Les arguments
chaîne de caractères doivent être fournis entre quotes simples
`'` ou doubles `"`.

La commande `plot` permet de tracer des courbes. Les données
sont lues depuis un fichier à raison d'un couple abscisse/ordonnée
par ligne comme dans cet exemple basique, fichier `ex1.dat`
(les `#` introduisent des commentaires) :

```
# taille duree
10   540.25
12   398.25
16   653.75
```
que l'on donne en paramètre à la commande `plot` :

```
gnuplot> plot "ex1.dat"
```

Il est possible de désigner les colonnes à considérer dans un fichier en
comportant plus de deux comme celui-ci (`ex2.dat`) :

```
# taille duree vitesse
10   540.25  56
12   398.25  35
16   653.75  21
```
On utilise alors l'option `using` de la commande
`plot` :

```
gnuplot> set xlabel "taille en lignes"
gnuplot> set ylabel "vitesse en octets/s"
gnuplot> plot "ex2.dat" using 1:3
```

L'exemple suivant, plus complet, introduit d'autres commandes et
options. On utilise ici le fait qu'il est possible d'invoquer
`gnuplot` avec en paramètre un fichier comportant des
commandes.

```sh
$ cat run.gnu
set title "Temps et vitesse d'execution"
set logscale x
set xlabel "taille en lignes"
set logscale y
set ylabel "temps en s ou vitesse en octets/s"
set style data linespoints
plot "ex2.dat" using 1:2 title "temps", \
     "ex2.dat" using 1:3 title "vitesse"
pause -1  "Hit return to continue"
$ gnuplot run.gnu
```

