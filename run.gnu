set title "Temps et vitesse d'execution mcat fsync"
set logscale x
set xlabel "taille buffer"
set logscale y
set ylabel "temps en s ou vitesse en octets/s"
set style data linespoints
plot "mcat-fsync.dat" using 1:1 title "taille", \
     "mcat-fsync.dat" using 1:2 title "vitesse"
pause -1  "Hit return to continue"
