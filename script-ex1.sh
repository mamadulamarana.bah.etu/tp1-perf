#!/bin/bash
# Exemple de script shell qui mesure le temps d'exécution de
# "macommande" dans les cas où la variable d'environnement
# MCAT_BUFSIZ vaut 1, 16 et 256

export MCAT_BUFSIZ
#MCAT_BUFSIZ=1   /usr/bin/time -f '%e %U %S' ./mcat-fsync input.txt
#MCAT_BUFSIZ=16  /usr/bin/time -f '%e %U %S' ./mcat-fsync input.txt
#MCAT_BUFSIZ=256 /usr/bin/time -f '%e %U %S' ./mcat-fsync input.txt
