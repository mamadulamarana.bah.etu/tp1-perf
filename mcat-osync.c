#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

int mcat_scs(char * path) {
    setenv("MCAT_BUFSIZ", "16", 1);

    char * var_env = getenv("MCAT_BUFSIZ");
    int buf_size = atoi(var_env);

    if (buf_size < 0) {
        perror("taille du tampon n'est pas bien initialisé");
        exit(-1);
    }

    int fd = open(path, O_RDONLY | O_SYNC, 0644);
    if (fd < 0) {
        printf("le fichiern'a pas été bien ouvert");
        exit(-1);
    }

    char * buffer = malloc(buf_size);
    if (buffer == NULL) {
        perror("le tampon est vide \n");
        exit(-1);
    }

    int reader = read(fd, buffer, buf_size);
    while (reader) {
        int writer = write(STDOUT_FILENO, buffer, reader);
        if (writer <= 0) {
            return -1;
        }

        reader = read(fd, buffer, buf_size);
    }

    if (reader < 0) {
        printf("lecture du fichier impossible");
        close(fd);
        free(buffer);
        exit(-1);
    }

    free(buffer);
    close(fd);
    return 0;
}

int main(int argc, char * argv[]) {
    if (argc == 0) {
        printf("mettez le nom du fichier");
        return 0;
    }
    char * path = argv[1];
    int cat = mcat_scs(path);
}