#include <stdlib.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>

int mcat_lib(const char * path) {

    FILE *file = fopen(path, "r");
    
    if (file == NULL) {
        printf("le fichiern'a pas été bien ouvert");
        exit(-1);
    }
    int reader = fgetc(file);
    while (reader != EOF) {
        int writter = fputc(reader, stdout);
        if (writter < 0) {
            fclose(file);
            exit(-1);
        }
        reader = fgetc(file);
    }

    fclose(file);
    return 0;
}

int main(int argc, char * argv[]) {
    if (argc == 0) {
        printf("mettez le nom du fichier");
        return 0;
    }
    char * path = argv[1];
    int cat = mcat_lib(path);
}